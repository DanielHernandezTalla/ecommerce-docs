import React from "react";
import clsx from "clsx";
import styles from "./styles.module.css";

const FeatureList = [
  {
    title: "Aplicación web",
    img: "/img/web.png",
    description: (
      <>
        La aplicación web tiene uso administrativo y para dar seguimiento a los
        pedidos de los clientes.
      </>
    ),
  },
  {
    title: "Aplicación movile",
    img: "/img/mobile.jpg",
    description: <>Aplicación para hacer pedidos al ecommerce.</>,
  },
  {
    title: "API REST",
    img: "/img/api.png",
    description: <>Es la API para la aplicación movil del ecommerce.</>,
  },
];

function Feature({ img, title, description }) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        <img src={img} height="200px" alt="Logo"/>
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
