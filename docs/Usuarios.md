---
sidebar_position: 1
---

# Roles de usuarios

Los roles de usuario, son de vital importancia, para asignar a cada usuario lo que puede o no pude hacer, lo roles son: **Cliente**, **Ejecutivo**, **Armador**, **Admin**, **Auditor** y **Facturista**.

| Roles  | Descripción  | Aplicación |
|:------------- |:---------------| :-------------|
| Admin     | Usuario encargado de administrar el sistema completo | Ecommerce WEB |
| Auditor   | Usuario que se encarga de los ajustes de inventario | Ecommerce WEB |
| Ejecutivo | Usuario que se encarga de dar seguimiento a los pedidos, además de tener acceso a ver el inventario de la aplicación. | Ecommerce WEB |
| Armador   | Usuario también llamado almacenista que se encarga del almacén, tiene acceso a los módulos de recepción de pedidos, al inventario y da seguimiento al pedido del cliente. | Ecommerce WEB |
| Facturista| Persona que tiene acceso al corte diario | Ecommerce WEB |
| Cliente   | Usuario que hace los pedidos, esta persona hace uso de la Ecommerce APP móvil.        | Ecommerce APP móvil|
