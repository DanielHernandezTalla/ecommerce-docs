# Articulos

El catalogo de articulos, es usado para administrar todos los articulos de todos, este catalogo es la base del catalogo para asignar articulos a los centros de venta.


> __¿Quién  lo usa?__
> Admin

## Descripción

Un articulo cuente con:

- Código: código de articulo
- Nombre: nombre del articulo
- Descripción corta: para escribir una descripción corta
- Descripción corta 2: en caso de que la descripción corta sea muy larga, aqui se pone el resto
- Descripción IVA: se usa para dar una descripción del IVA
- IVA: para agregar IVA
- Categoría: se da una categoria al articulo, cada articulo solo pertenece a una categoría
- Paquete: se asigna un paquete en caso de ser necesario
- Unidad de medida: se asigna una unidad de media, generalmente se usan kilos
- Peso promedio: se usa para dar un peso promedio a los articulos y por medio de este se calculan las piezas
- Imagen: se asigna una imagen a todos los articulos
- Descripción general: aqui se da una descripción general del articulo y puede ser del largo que sea necesario
- Activo: se indica si es articulo esta activo o no