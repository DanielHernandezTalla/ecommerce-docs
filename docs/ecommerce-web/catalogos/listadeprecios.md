# Lista de precios

El catalogo de lista de precios, administra las listas de precios. Las listas de precios se usan para asignarselas a una ciudad y a su vez, estos precios son tomados por todos los centro de venta de esa ciudad. Y en caso de modificar un precio en la lista, se vera afectado en todas las ciudades a la que la lista pertenece y a su ves en todos los centro de venta de esa ciudad.


> __¿Quién  lo usa?__
> Admin

## Descripción

El catalogo de lista de precios costa de un nombre para la lista de precios y un estado, activo o no activo.

- Nombre lista precios
- Activo