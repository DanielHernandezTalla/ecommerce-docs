# Usuarios

El catálogo usuario de usuarios se encarga de administrar los usuarios de la aplicación. Puedes crear, actualizar, eliminar y leer usuarios que no sean de tipo cliente, ya que esos se administran desde la aplicación móvil.


> __¿Quién  lo usa?__
> Admin

## Descripción

Un usuario tiene que tener nombre, tipo de usuario (Rol), puede pertenecer a un centro de venta, se le debe asignar una contraseña y si esta activo o no.

- Nombre de usuario
- Tipo de usuario (Rol)
- Centro de venta (Opcional)
- Contraseña 
- Confirmar contraseña
- Activo