# Inventario

El catálogo de inventario es usado para ver el inventario de la aplicación, ademas de hacer ajustes de inventario. 


> __¿Quién  lo usa?__
> auditor, ejecutivo y armador

## Descripción

El catalogo para mostrar inventario, puede ser accesido por auditor, ejecutivo y armador, sin embargo, el ajuste de inventario solo se puede hacer por el auditor. 

:::note Nota

Al hacer el ajuste de inventario, es importante que siempre se ingresen pienzas enteras, de lo contrario el sistema no te permitira hacerlo. 

:::