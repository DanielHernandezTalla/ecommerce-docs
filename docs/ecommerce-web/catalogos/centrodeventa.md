# Centro de venta

El catálogo de centro de ventas, se usa para administrar los centros de ventas.


> __¿Quién  lo usa?__
> Admin

## Descripción

Un centro de venta tiene nombre de centro de venta, la ciudad a la que pertenece, almacen, tipo de orden oracle, el costo de envio para ese centro de venta y si esta activo o no.

- Nombre centro de venta
- Ciudad
- Almacen
- Tipo orden oracle
- Costo envio
- Activo
