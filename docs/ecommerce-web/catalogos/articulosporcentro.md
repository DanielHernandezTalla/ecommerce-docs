# Articulos por centro de venta

El catalogo de articulos por centro de venta, administra los articulos que puede vender cada centro de venta.


> __¿Quién  lo usa?__
> Admin

## Descripción

Para usar este catalogo, primero se selecciona un centro de venta, despues, se le agregan articulos, estos articulos, se pueden eliminar despues de agregados. Para modificar el estado de activo a no activo, y para guardar los cambio se utiliza el boton de actualizar estatus.

![Catalogo articulos por centro de venta](./img/artoculosporcentro.png)