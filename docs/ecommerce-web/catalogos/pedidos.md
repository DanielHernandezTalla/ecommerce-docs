# Pedidos

El catalogo de pedidos, es usado para dar seguimiento a una pedido de un cliente. Primero el pedido esta en pendiente, despues se pasa al almacenista para que este lo prepare, se envia y al final estan los pedidos entregados. 


> __¿Quién  lo usa?__
> _ejecutivo_ y _armador_

## Descripción

Catalogo para dar seguimiento a los pedidos, y ver el historial de pedidos.

::::note NOTA

El _ejecutivo_ y el _armador_ ven diferentes estados del pedido, el _ejecutivo_ puede ver los pendientes, enviados y finalizados, mientras que el _armador_ puede ver los que estan en preparación.

::::