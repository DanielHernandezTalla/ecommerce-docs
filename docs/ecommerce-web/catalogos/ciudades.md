# Ciudades

El catálogo de ciudades, se usa para administrar las ciudades en la aplicación.


> __¿Quién  lo usa?__
> Admin

## Descripción

Éste catálogo se crea con el nombre de la ciudad, a la ciudad se le asigna una lista de precios, una hora de entrada, por la cual se trabajara, una hora de salida y si esta activa o no.

- Nombre de la ciudad
- Lista de precios
- Hora de entrada
- Hora de salida 
- Activo

:::note Nota

Es importante que el estado de la ciudad este activo, solo para las ciudades donde se da el servicio, ya que de esto depende quien se puede registrar en la aplicación móvil. 

:::