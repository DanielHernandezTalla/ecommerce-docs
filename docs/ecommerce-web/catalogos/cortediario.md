# Corte diario

El catalogo de corte diario, solo muestra el corte diario del centro de venta al que pertenece el facturista.


> __¿Quién  lo usa?__
> Facturista

## Descripción


:::note Nota

Para ver el corte diario de otras fechas, se selecciona la fecha a buscar en el input.

:::