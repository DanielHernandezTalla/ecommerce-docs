# Roles

El catálogo de roles, se usa para administrar los roles que existen en la aplicación.


> __¿Quién  lo usa?__
> Admin

## Descripción

Un rol solo consta de dos atributos, como lo es el nombre del rol y si esta activo o no.

- Nombre del rol
- Activo

:::caution Cuidado

Si cambias el nombre de un rol, el cual es usado en la Ecommerce Web, el sistema dejara de funcionar para esos roles, ya que el routing depende del nombre del rol para que redirija al lugar correcto.

:::