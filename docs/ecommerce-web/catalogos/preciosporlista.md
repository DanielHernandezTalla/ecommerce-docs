# Precios por lista

El catalogo de precios por lista, muestra los precios de cada lista de precios, es decir, muestra los articulos de cada lista de precios y le puedes asignar un nuevo precio a los productos seleccionados. 


> __¿Quién  lo usa?__
> Admin

## Descripción

En este catalogo solo se pueden cambiar los precios a los productos por lista.

::::note NOTA

Si cambias los precios en una lista, esta afectara los precios en todas las ciudades que tienen esta lista y a su ves a los centros de venta que pertencen a esas ciudades.

::::