# Recepción de producto

El catalogo de recepcion de producto, sirve tanto para recepcionar producto que viene de _Paking List_ o producto manual.


> __¿Quién  lo usa?__
> Armador

## Descripción


:::note Nota

La captura de productos, siempre tiene que ser por piezas exactas. Al recepcionar un detalle de producto, los productos de los checks no seleccionados no se recepcionaran.

:::